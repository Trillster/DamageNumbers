#library "DMGNUM"
#include "zcommon.acs"

#define TCLR_DEFAULT 230
#define CLR_TEXT 850

#define PLN_TID 1000
#define MAX_PLAYERS 64
#define MAX_TEAM_ID 3 // 0-3 are valid teams

#define ONE_TIC 1873
#define SYNC_DURATION 35

script "damagenumbers_notifier" OPEN CLIENTSIDE {
	if(GetUserCvar(ConsolePlayerNumber(), "trill_dm_notification")) {
		Log (
			s:StrParam (
				s:"\c", s:GetUserCvarString(ConsolePlayerNumber(),"trill_dm_texthighlights"), 
				s:"Damage display \c", s:GetUserCvarString(ConsolePlayerNumber(),"trill_dm_textnumbers"), 
				s:"enabled. Type \c", s:GetUserCvarString(ConsolePlayerNumber(),"trill_dm_texthighlights"), 
				s:"ddoptions\c", s:GetUserCvarString(ConsolePlayerNumber(),"trill_dm_textnumbers"), 
				s:" in console for available options."
			)
		);
	}
}

function bool shouldShowDD(str client, str server) {
	int cpn = ConsolePlayerNumber();
	return GetUserCvar(cpn, client) && (GetCvar(server) || (!GetCvar(server) && PlayerIsSpectator(cpn) == 1));
}

script "DamageNumbers_Open" OPEN {
	if(!(isCountdownMode() && GetGamemodeState() == GAMESTATE_INPROGRESS)) {
		for(int i = 0; i < MAX_PLAYERS; i++) {
			SetCvar(StrParam(s:"Player_", i:i, s:"_DamageDone"), 0);
			SetCvar(StrParam(s:"Player_", i:i, s:"_KnocksTaken"), 0);

			SetCustomPlayerValue("DD_Damage", i, 0);
			SetCustomPlayerValue("DD_Knocks", i, 0);
		}
	}
}

function bool isCountdownMode(void) {
	return StrICmp(GetCurrentGameMode(),"TeamLMS")==0 
		|| StrICmp(GetCurrentGameMode(),"LastManStanding")==0 
		|| StrICmp(GetCurrentGameMode(),"Duel")==0
		|| StrICmp(GetCurrentGameMode(),"Possession")==0
		|| StrICmp(GetCurrentGameMode(),"TeamPossession")==0;
}

script "DamageNumbers_Disconnect" (int goner) DISCONNECT {
	SetCvar(StrParam(s:"Player_", i:goner, s:"_DamageDone"), 0);
	SetCvar(StrParam(s:"Player_", i:goner, s:"_KnocksTaken"), 0);

	SetCustomPlayerValue("DD_Damage", goner, 0);
	SetCustomPlayerValue("DD_Knocks", goner, 0);
}

script "DamageNumbers_Watcher" OPEN CLIENTSIDE {
	int cpn = ConsolePlayerNumber();
	int validCam = 0;

	while(true) {
		validCam = TryDamageDisplayFunc(cpn, validCam);
		Delay(1);
	}
}

// This more or less copies the internal assist display logic, but
// this mod is meant to be compatible with pre-V6B as well...
function int TryDamageDisplayFunc(int cpn, int validCam) {
	int cam = CheckPlayerCamera(cpn);
	if((cam > 0 && cam < PLN_TID) || cam >= PLN_TID + MAX_PLAYERS) { // we're watching something but not a player...
		if(validCam != 0 && PlayerInGame(validCam - PLN_TID) && ThingCount(T_NONE, validCam) > 0) {
			cam = validCam; // we have a valid previous player to refer to
		} else if (validCam != 0 && PlayerInGame(validCam - PLN_TID)) {
			return validCam; // can't do anything this instance around because respawning player with no TID yet, but soon...
		} else {
			return 0; // no valid last player and we're not watching a player, so nothing to do here.
		}
	}
	if(cam >= PLN_TID && cam < PLN_TID + MAX_PLAYERS) {
		DoDamageDisplayFunc(cam, cpn);
	}
	return cam;
}

function void DoDamageDisplayFunc(int cam, int cpn) {
	SetHudSize(600, 450, 0);

	if(GetUserCvar(cpn,"trill_dm_scoreboarddisable")) {
		int buttons = GetPlayerInput(cpn, INPUT_BUTTONS);
	} else {
		buttons = 0;
	}

	if(!(buttons & BT_SHOWSCORES) && shouldShowDD("trill_dm_acsdisplay", "trill_dd_sv_statdisplay")) {
		if(GetUserCvar(cpn,"trill_dm_showlasthit")) {
			str lastHit = StrParam(s:"\n\c", s:GetUserCvarString(cpn,"trill_dm_texthighlights"), s:GetUserCvarString(cpn,"trill_dm_lasthitmessage"), s:" \c", s:GetUserCvarString(cpn,"trill_dm_textnumbers"), i:CheckActorInventory(cam, "DM_LastHit"), s:" (", i:CheckActorInventory(cam, "DM_LastHitAmount"), s:")");
		} else { lastHit = ""; }

		if(GetUserCvar(cpn,"trill_dm_showdamagedone")) {
			str dmgDone = StrParam(s:"\n\c", s:GetUserCvarString(cpn,"trill_dm_texthighlights"), s:GetUserCvarString(cpn,"trill_dm_damagemessage"), s:" \c", s:GetUserCvarString(cpn,"trill_dm_textnumbers"), i:CheckActorInventory(cam, "DM_DamageDone"));
		} else { dmgDone = ""; }

		if(GetUserCvar(cpn,"trill_dm_showtotaldamage")) {
			str totalDmg = StrParam(s:"\n\c", s:GetUserCvarString(cpn,"trill_dm_texthighlights"), s:GetUserCvarString(cpn,"trill_dm_totalmessage"), s:" \c", s:GetUserCvarString(cpn,"trill_dm_textnumbers"), i:GetCvar(StrParam(s:"Player_", i:cam - PLN_TID, s:"_DamageDone")));
		} else { totalDmg = ""; }

		if(GetUserCvar(cpn,"trill_dm_showfrags")) {
			str frags = StrParam(s:"\n\c", s:GetUserCvarString(cpn,"trill_dm_texthighlights"), s:GetUserCvarString(cpn,"trill_dm_fragsmessage"), s:" \c", s:GetUserCvarString(cpn,"trill_dm_textnumbers"), i:GetPlayerScore(cam - PLN_TID, SCORE_FRAGS));
		} else { frags = ""; }

		if(GetUserCvar(cpn,"trill_dm_showlastknock")) {
			str lastKnock = StrParam(s:"\n\c", s:GetUserCvarString(cpn,"trill_dm_texthighlights"), s:GetUserCvarString(cpn,"trill_dm_lastknockmessage"), s:" \c", s:GetUserCvarString(cpn,"trill_dm_textnumbers"), i:CheckActorInventory(cam, "DM_LastKnock"), s:" (", i:CheckActorInventory(cam, "DM_LastKnockAmount"), s:")");
		} else { lastKnock = ""; }

		if(GetUserCvar(cpn,"trill_dm_showknockstaken")) {
			str kckDone = StrParam(s:"\n\c", s:GetUserCvarString(cpn,"trill_dm_texthighlights"), s:GetUserCvarString(cpn,"trill_dm_knocksmessage"), s:" \c", s:GetUserCvarString(cpn,"trill_dm_textnumbers"), i:CheckActorInventory(cam, "DM_KnocksTaken"));
		} else { kckDone = ""; }

		if(GetUserCvar(cpn,"trill_dm_showtotalknocks")) {
			str totalKck = StrParam(s:"\n\c", s:GetUserCvarString(cpn,"trill_dm_texthighlights"), s:GetUserCvarString(cpn,"trill_dm_totalknocksmessage"), s:" \c", s:GetUserCvarString(cpn,"trill_dm_textnumbers"), i:GetCvar(StrParam(s:"Player_", i:cam - PLN_TID, s:"_KnocksTaken")));
		} else { totalKck = ""; }

		if(GetUserCvar(cpn,"trill_dm_showdeaths")) {
			str deaths = StrParam(s:"\n\c", s:GetUserCvarString(cpn,"trill_dm_texthighlights"), s:GetUserCvarString(cpn,"trill_dm_deathsmessage"), s:" \c", s:GetUserCvarString(cpn,"trill_dm_textnumbers"), i:GetPlayerScore(cam - PLN_TID, SCORE_DEATHS));
		} else { deaths = ""; }

		HudMessage ( 
			s:lastHit,
			s:dmgDone,
			s:totalDmg,
			s:frags,
			s:"\n",
			s:lastKnock,
			s:kckDone,
			s:totalKck,
			s:deaths;
			HUDMSG_PLAIN|HUDMSG_ALPHA, -9999, CR_WHITE, 0.1, 225.1, ONE_TIC, max(min(GetUserCvar(cpn,"trill_dm_displayopacity"), 1.0), 0.0)
		);
	}
}

script "DamageNumbers_Enter" ENTER {
	int team = GetPlayerInfo(PlayerNumber(),PLAYERINFO_TEAM);
	if(team == 255) { team = 4; }

	int oldHP = GetActorProperty(ActivatorTID(), APROP_Health);

	while(GetActorProperty(ActivatorTID(), APROP_Health) > 0 && PlayerInGame(PlayerNumber())) {
		int diff = GetActorProperty(ActivatorTID(), APROP_Health) - oldHP;

		if(diff < 0) {
			oldHP = GetActorProperty(ActivatorTID(),APROP_Health);
		} else if(diff > 0) {
			oldHP = GetActorProperty(ActivatorTID(),APROP_Health);
			if(diff > 9999) { diff = 9999; }

			ACS_NamedExecuteAlways("DamageNumbers_HealNumber", 0, diff, team < 4); 
		}

		Delay(1);
	}
}

script "DamageNumbers_Respawn" RESPAWN {
	HudMessage(s:""; HUDMSG_PLAIN, -9998, CR_GOLD, 0.5, 0.75, 5.0);
	ACS_NamedExecuteAlways("DamageNumbers_Enter", 0);
}

script "DamageNumbers_Death" DEATH {
	NamedExecuteClientScript("DamageNumbers_Printout", PlayerNumber(), PlayerNumber());
}

script "DamageNumbers_Printout" (int playerNum) CLIENTSIDE {
	int cpn = ConsolePlayerNumber();
	if(GetUserCvar(cpn, "trill_dm_showdeathprintout") && GetCvar("trill_dd_sv_deathprintout")){
		HudMessage(
			s:StrParam(s:"\c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"Your last hit did \c", s:GetUserCvarString(cpn, "trill_dm_textnumbers"), 
			i:CheckActorInventory(playerNum+PLN_TID,"DM_LastHit"), 
			s:" \c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"damage\n", 
			s:"\c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"You did \c", s:GetUserCvarString(cpn, "trill_dm_textnumbers"), 
			i:CheckActorInventory(playerNum+PLN_TID,"DM_DamageDone"), 
			s:" \c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"damage that life\n", 
			s:"\c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"Your total damage done is: \c", s:GetUserCvarString(cpn, "trill_dm_textnumbers"), 
			i:GetCvar(StrParam(s:"Player_", i:playerNum, s:"_DamageDone")),
			s:"\n\c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"You have \c", s:GetUserCvarString(cpn, "trill_dm_textnumbers"), 
			i:GetPlayerScore(playerNum,SCORE_FRAGS),
			s:" \c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"frag(s)"),
			s:"\n\n\c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"Your last knock was \c", s:GetUserCvarString(cpn, "trill_dm_textnumbers"), 
			i:CheckActorInventory(playerNum+PLN_TID,"DM_LastKnock"), 
			s:" \c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"damage\n", 
			s:"\c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"You took \c", s:GetUserCvarString(cpn, "trill_dm_textnumbers"), 
			i:CheckActorInventory(playerNum+PLN_TID,"DM_KnocksTaken"), 
			s:" \c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"damage that life\n", 
			s:"\c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"Your total damage taken is: \c", s:GetUserCvarString(cpn, "trill_dm_textnumbers"), 
			i:GetCvar(StrParam(s:"Player_", i:playerNum, s:"_KnocksTaken")),
			s:"\n\c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"You have \c", s:GetUserCvarString(cpn, "trill_dm_textnumbers"), 
			i:GetPlayerScore(playerNum,SCORE_DEATHS),
			s:" \c", s:GetUserCvarString(cpn, "trill_dm_texthighlights"), 
			s:"deaths(s)";
			HUDMSG_PLAIN|HUDMSG_LOG|HUDMSG_ALPHA, -9998, CR_GOLD, 1.5, 0.75, 5.0, max(min(GetUserCvar(cpn, "trill_dm_printoutopacity"), 1.0), 0.0)
		);
	}
}

int legacyHitConfirmLoaded = -1;
function bool useLegacyHitConfirms(void) {
	if(legacyHitConfirmLoaded < 0) {
		int u = UniqueTID();
		legacyHitConfirmLoaded = SpawnForced("MegaAnyScorePillar", 0, 0, 0, u, 0);
		if (legacyHitConfirmLoaded) {
			Thing_Remove(u);
		}
	}

	return !legacyHitConfirmLoaded;
}

int PlayerLastHitNoise[MAX_PLAYERS][2];

script "DamageNumbers_EndRoundSync" (int pln, int which, int target, int dur) {
	int per = (target - GetCustomPlayerValue(which, pln)) / dur;
	for(int i = 0; i < dur - 1; i++) {
		SetCustomPlayerValue(which, pln, GetCustomPlayerValue(which, pln) + per);
		Delay(1);
	}
	SetCustomPlayerValue(which, pln, target);
}

script "DamageNumbers_Trigger" (int type, int dmg, int arg2) EVENT {
	if(type == GAMEEVENT_ROUND_ENDS) {
		for(int i = 0; i < MAX_PLAYERS; i++) {
			if(PlayerInGame(i)){
				NamedExecuteClientScript("DamageNumbers_Printout", i, i);
			}

			if(!GetCvar("trill_dd_sv_realtimedamage")) {
				ACS_NamedExecuteWithResult("DamageNumbers_EndRoundSync", i, "DD_Damage", GetCvar(StrParam(s:"Player_", i:i, s:"_DamageDone")), SYNC_DURATION);
			}

			if(!GetCvar("trill_dd_sv_realtimeknocks")) {
				ACS_NamedExecuteWithResult("DamageNumbers_EndRoundSync", i, "DD_Knocks", GetCvar(StrParam(s:"Player_", i:i, s:"_KnocksTaken")), SYNC_DURATION);
			}
		}
	} else if(type == GAMEEVENT_ACTOR_DAMAGED) {
		dmg = GetEventResult();

		if(ActivatorTID() < PLN_TID || ActivatorTID() >= PLN_TID + MAX_PLAYERS || dmg <= 0){ terminate; }

		int victim_hp = GetActorProperty(0, APROP_Health);
		dmg = min(victim_hp - (CheckInventory("PlayerPropertyBuddha") > 0), dmg);

		if(GetGamemodeState() != GAMESTATE_COUNTDOWN) {
			int knk_val = GetCvar(StrParam(s:"Player_", i:PlayerNumber(), s:"_KnocksTaken")) + dmg;
			SetCvar (
				StrParam(s:"Player_", i:PlayerNumber(), s:"_KnocksTaken"),
				knk_val
			);

			if(GetCvar("trill_dd_sv_realtimeknocks")) {
				SetCustomPlayerValue("DD_Knocks", PlayerNumber(), knk_val);
			}
		}
		
		GiveInventory("DM_KnocksTaken", dmg);

		if(CheckInventory("DM_LastKnockCheck") > 0) {
			GiveInventory("DM_LastKnockAmount", 1);
			GiveInventory("DM_LastKnock", dmg);
			if(victim_hp > 0) {
				GiveInventory("DM_LastKnockCheck", 1);
			}
		} else {
			SetInventory("DM_LastKnockAmount",1);
			SetInventory("DM_LastKnock", dmg);
			if(victim_hp > 0){
				GiveInventory("DM_LastKnockCheck", 1);
			}
		}

		int oldTID = ActivatorTID();
		SetActivator(0, AAPTR_DAMAGE_SOURCE);

		int shooter_team = 4; // default for World damage

		if(ActivatorTID() >= PLN_TID && ActivatorTID() < PLN_TID + MAX_PLAYERS && ActivatorTID() != oldTID) {
			if(useLegacyHitConfirms()) {
				if(dmg >= victim_hp && Timer() != PlayerLastHitNoise[PlayerNumber()][1]) {
					PlayerLastHitNoise[PlayerNumber()][1] = Timer();
					ACS_NamedExecuteAlways("DamageNumbers_FragNoise", 0);
				} else if (Timer() != PlayerLastHitNoise[PlayerNumber()][0]) {
					PlayerLastHitNoise[PlayerNumber()][0] = Timer();
					ACS_NamedExecuteAlways("DamageNumbers_HitNoise", 0);
				}
			}

			if(GetGamemodeState() != GAMESTATE_COUNTDOWN) {
				int dmg_val = GetCvar(StrParam(s:"Player_", i:PlayerNumber(), s:"_DamageDone")) + dmg;
				SetCvar (
					StrParam(s:"Player_", i:PlayerNumber(), s:"_DamageDone"),
					dmg_val
				);

				if(GetCvar("trill_dd_sv_realtimedamage")) {
					SetCustomPlayerValue("DD_Damage", PlayerNumber(), dmg_val);
				}
			}

			if(GetActorProperty(0, APROP_Health) > 0 && PlayerInGame(PlayerNumber())) {
				GiveInventory("DM_DamageDone", dmg);
				if(CheckInventory("DM_LastHitCheck") > 0){
					GiveInventory("DM_LastHitAmount", 1);
					GiveInventory("DM_LastHit", dmg);
					GiveInventory("DM_LastHitCheck", 1);
				} else {
					SetInventory("DM_LastHitAmount", 1);
					SetInventory("DM_LastHit", dmg);
					GiveInventory("DM_LastHitCheck", 1);
				}
			}

			shooter_team = GetPlayerInfo(PlayerNumber(), PLAYERINFO_TEAM);
		}

		shooter_team = min(shooter_team, 4); // clamp 255 to 4
		
		int text_color = shooter_team + TCLR_DEFAULT;
		if(shooter_team > MAX_TEAM_ID){ text_color =  CLR_TEXT; }
			
		SetActivator(oldTID);

		ACS_NamedExecuteAlways("DamageNumbers_DamageNumber", 0, min(dmg, 9999), text_color);
	}
}

script "DamageNumbers_DamageNumber" (int dmg, int text_color) CLIENTSIDE {
	if(shouldShowDD("trill_dm_damagenumbers", "trill_dd_sv_damagenumbers")) {
		int newTID = UniqueTID();
		SpawnForced("DamageNumberWatcher", GetActorX(0), GetActorY(0), GetActorZ(0), newTID);
		SetUserVariable(newTID, "user_amount", dmg);
		SetUserVariable(newTID, "user_color", text_color);
		Thing_ChangeTID(newTID, 0);
	}
}

script "DamageNumbers_GetValue" (int type) CLIENTSIDE {
	if(type == 0){
		int res = GetUserVariable(0,"user_random");
	} else {
		res = GetUserVariable(0,"user_angle");
	}

	SetResultValue(res);
}

script "DamageNumbers_SpawnNumbers" (int type, int number) CLIENTSIDE {
	str typeString = "Damage";
	if(type == 1){ typeString = "Heal"; }

	if(number < 10) {
		GiveInventory(StrParam(s:"Spawn", s:typeString, s:"Number", i:number),1);
	} else if(number < 100) {
		GiveInventory(StrParam(s:"Spawn", s:typeString, s:"NumberX", i:number % 10),1);
		number /= 10;
		GiveInventory(StrParam(s:"Spawn", s:typeString, s:"Number", i:number % 10, s:"X"),1);
	} else if(number < 1000) {
		GiveInventory(StrParam(s:"Spawn", s:typeString, s:"NumberXX", i:number % 10),1);
		number /= 10;
		GiveInventory(StrParam(s:"Spawn", s:typeString, s:"NumberX", i:number % 10, s:"X"),1);
		number /= 10;
		GiveInventory(StrParam(s:"Spawn", s:typeString, s:"Number", i:number % 10, s:"XX"),1);
	} else {
		GiveInventory(StrParam(s:"Spawn", s:typeString, s:"NumberXXX", i:number % 10),1);
		number /= 10;
		GiveInventory(StrParam(s:"Spawn", s:typeString, s:"NumberXX", i:number % 10, s:"X"),1);
		number /= 10;
		GiveInventory(StrParam(s:"Spawn", s:typeString, s:"NumberX", i:number % 10, s:"XX"),1);
		number /= 10;
		GiveInventory(StrParam(s:"Spawn", s:typeString, s:"Number", i:number % 10, s:"XXX"),1);
	}
}

script "DamageNumbers_HealNumber" (int heal, int team) CLIENTSIDE {
	if(shouldShowDD("trill_dm_healnumbers", "trill_dd_sv_healnumbers")) {
		SetInventory("DM_NumberAmount", heal);
		if(team) {
			GiveInventory("SpawnHealNumberWatcherTeam",1);
		} else {
			GiveInventory("SpawnHealNumberWatcher",1);
		}
	}
}

script "DamageNumbers_GrabAmount" (void) {
	SetActivatorToTarget(0);
	int result = CheckInventory("DM_NumberAmount");
	SetResultValue(result);
}

int fragsoundVol = 0.0;
int volFragCvarStore = 0.0;
script "DamageNumbers_FragNoise" (void) CLIENTSIDE {
	if(shouldShowDD("trill_dm_fragsound", "trill_dd_sv_fragsound")) {
		int volume = GetUserCvar(ConsolePlayerNumber(),"trill_dm_legacyfragsoundvolume");
		if(volFragCvarStore != volume) {
			fragsoundVol = FixedMul(127.0, volume) >> 16;
			volFragCvarStore = volume;
		}

		LocalAmbientSound("dd/acs/fragnoise", fragsoundVol);
	}
}

int hitsoundVol = 0.0;
int volHitCvarStore = 0.0;
script "DamageNumbers_HitNoise" (void) CLIENTSIDE {
	if(shouldShowDD("trill_dm_hitsound", "trill_dd_sv_hitsound")) {
		int volume = GetUserCvar(ConsolePlayerNumber(),"trill_dm_legacyhitsoundvolume");
		if(volHitCvarStore != volume) {
			hitsoundVol = FixedMul(127.0, volume) >> 16;
			volHitCvarStore = volume;
		}

		LocalAmbientSound("dd/acs/hitnoise", hitsoundVol);
	}
}

function void SetActorInventory(str tid, str inv, int amt) {
	int oldamt = CheckActorInventory(tid, inv);

	if(oldamt > amt) {
		TakeActorInventory(tid,inv, oldamt - amt);
	} else {
		GiveActorInventory(tid,inv, amt - oldamt);
	}
}

function void SetInventory(str inv, int amt) {
	int oldamt = CheckInventory(inv);

	if(oldamt > amt) {
		TakeInventory(inv, oldamt - amt);
	} else {
		GiveInventory(inv, amt - oldamt);
	}
}

function int max(int x, int y) {
	if(x > y) {
		return x;
	}

	return y;
}

function int min(int x, int y) {
	if(x < y) {
		return x;
	}

	return y;
}